package html_information.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GoTo
 */
@WebServlet("/GoTo")
public class GoTo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GoTo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
		// Determine the site where they want to go
		String site = req.getPathInfo();
		String query = req.getQueryString();
		
		// Handle a bad request
		if (site == null) {
		res.sendError(res.SC_BAD_REQUEST, "Extra path info required");
		}
		
		// Cut off the leading "/" and append the query string
		// We're assuming the path info URL is always absolute
		String url = site.substring(1) + (query == null ? "" : "?" + query);
		
		// Log the requested URL and redirect
		log(url);  // or write to a special file
		res.sendRedirect(url);
		}

}
