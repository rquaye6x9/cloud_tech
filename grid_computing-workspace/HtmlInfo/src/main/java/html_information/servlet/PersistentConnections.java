package html_information.servlet;

import java.io.IOException;
import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

/**
 * Servlet implementation class PersistentConnections
 */
@WebServlet("/PersistentConnections")
public class PersistentConnections extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

		res.setContentType("text/html");
		
		// Ask for a 16K byte response buffer; do not set the content length
		res.setBufferSize(16 * 1024);
		
		PrintWriter out = res.getWriter();
		out.println("<HTML>");
		out.println("<HEAD><TITLE>Hello World</TITLE></HEAD>");
		out.println("<BODY>");
		out.println("<BIG>Less than 16K of response body</BIG>");
		out.println("</BODY></HTML>");
		}

}
