package RetrieveInformation.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SnoopingHeaders
 */
@WebServlet("/SnoopingHeaders")
public class SnoopingHeaders extends HttpServlet {
	 public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/plain");
		PrintWriter out = res.getWriter();
		
		out.println("Request Headers:");
		out.println();
		Enumeration<String> names = req.getHeaderNames();
		while (names.hasMoreElements()) {
			String name = (String) names.nextElement();
			Enumeration<String> values = req.getHeaders(name);  // support multiple values
			if (values != null) {
				while (values.hasMoreElements()) {
				String value = (String) values.nextElement();
				out.println(name + ": " + value);
				}
			}
		}
	 }
}
