package RetrieveInformation.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;

/**
 * Servlet implementation class UploadTest
 */
@WebServlet("/UploadTest")
public class UploadTest extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		
		try {
		// Blindly take it on faith this is a multipart/form-data request
		
		// Construct a MultipartRequest to help read the information.
		// Pass in the request, a directory to save files to, and the
		// maximum POST size we should attempt to handle.
		// Here we (rudely) write to /tmp and impose a 50 K limit.
		MultipartRequest multi =
		new MultipartRequest(req, "/tmp", 50 *1024 * 1024,
		new com.oreilly.servlet.multipart.DefaultFileRenamePolicy());
		
		out.println("<HTML>");
		out.println("<HEAD><TITLE>UploadTest</TITLE></HEAD>");
		out.println("<BODY>");
		out.println("<H1>UploadTest</H1>");
		
		// Print the parameters we received
		out.println("<H3>Params:</H3>");
		out.println("<PRE>");
		Enumeration<String> params = multi.getParameterNames();
		while (params.hasMoreElements()) {
			String name = (String)params.nextElement();
			String value = multi.getParameter(name);
			out.println(name + " = " + value);
		}
		out.println("</PRE>");
		
		// Show which files we received
		out.println("<H3>Files:</H3>");
		out.println("<PRE>");
		Enumeration<String> files = multi.getFileNames();
		while (files.hasMoreElements()) {
		String name = (String)files.nextElement();
		String filename = multi.getFilesystemName(name);
		String original = multi.getOriginalFileName(name);
		String type = multi.getContentType(name);
		File f = multi.getFile(name);
		out.println("name: " + name);
		out.println("filename: " + filename);
		if (filename != null && !filename.equals(original)) {
		out.println("original file name: " + original);
		}
		out.println("type: " + type);
		if (f != null) {
		out.println("length: " + f.length());
		}
		out.println();
		}
		out.println("</PRE>");
		}
		catch (Exception e) {
		out.println("<PRE>");
		e.printStackTrace(out);
		out.println("</PRE>");
		}
		out.println("</BODY></HTML>");
	}

}
