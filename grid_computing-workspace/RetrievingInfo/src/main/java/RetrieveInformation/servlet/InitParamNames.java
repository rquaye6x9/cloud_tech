package RetrieveInformation.servlet;
import java.io.*;
import java.util.*;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;



/**
 * Servlet implementation class InitParamNames
 */
@WebServlet("/InitParamNames")
public class InitParamNames extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public void service(ServletRequest request, ServletResponse response)
                               throws ServletException, IOException {
    	response.setContentType("text/plain");
      PrintWriter out = response.getWriter();
     
      out.println("Init Parameters:");
      Enumeration<String> e = getInitParameterNames();
      while (e.hasMoreElements()) {
        String name = (String) e.nextElement();
        out.println(name + ": " + getInitParameter(name));
      }
    }
	
}
