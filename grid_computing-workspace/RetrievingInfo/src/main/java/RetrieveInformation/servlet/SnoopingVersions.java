package RetrieveInformation.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;


/**
 * Servlet implementation class SnoopingVersions
 */
@WebServlet("/SnoopingVersions")
public class SnoopingVersions extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException,IOException {
	res.setContentType("text/plain");
	PrintWriter out =res.getWriter();
	
	out.println("Servlet Version: " + VersionDetector.getServletVersion());
	out.println("Java Version: " + VersionDetector.getJavaVersion());
	}

}
