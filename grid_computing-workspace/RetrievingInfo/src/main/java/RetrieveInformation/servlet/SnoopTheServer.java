package RetrieveInformation.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;                                                    
import java.util.*;                                                   
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SnoopTheServer
 */
@WebServlet("/SnoopTheServer")
public class SnoopTheServer extends HttpServlet {
	public void service(ServletRequest req, ServletResponse res)       
            throws ServletException, IOException {  
	res.setContentType("text/plain");                                
	PrintWriter out = res.getWriter();                               
	
	ServletContext context = getServletContext();
	out.println("req.getServerName(): " + req.getServerName());      
	out.println("req.getServerPort(): " + req.getServerPort());      
	out.println("context.getServerInfo(): " + context.getServerInfo());
	out.println("getServerInfo() name: " +                           
	getServerInfoName(context.getServerInfo()));
	out.println("getServerInfo() version: " +                        
	getServerInfoVersion(context.getServerInfo()));
	out.println("context.getAttributeNames():");
	Enumeration<String> enumeration = context.getAttributeNames();
	while (enumeration.hasMoreElements()) {
	String name = (String) enumeration.nextElement();
	out.println("  context.getAttribute(\"" + name + "\"): " +
	    context.getAttribute(name));
	}
	}
	
	private String getServerInfoName(String serverInfo) {
	int slash = serverInfo.indexOf('/');
	if (slash == -1) return serverInfo;
	else return serverInfo.substring(0, slash);
	}
	
	private String getServerInfoVersion(String serverInfo) {
	// Version info is everything between the slash and the space
	int slash = serverInfo.indexOf('/');
	if (slash == -1) return null;
	int space = serverInfo.indexOf(' ', slash);
	if (space == -1) space = serverInfo.length();
	return serverInfo.substring(slash + 1, space);
	}
}
