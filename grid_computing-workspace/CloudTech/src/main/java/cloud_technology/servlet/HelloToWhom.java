package cloud_technology.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloToWhom
 */
@WebServlet("/HelloToWhom")
public class HelloToWhom extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloToWhom() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		   res.setContentType("text/html");
		    PrintWriter out = res.getWriter();

		    String name = req.getParameter("name");
		    out.println("<HTML>");
		    out.println("<HEAD><TITLE>Hello, " + name + "</TITLE></HEAD>");
		    out.println("<BODY>");
		    out.println("Hello, " + name);
		    out.println("</BODY></HTML>");
		  }

		  public String getServletInfo() {
		    return "A servlet that knows the name of the person to whom it's" + 
		           "saying hello";
		  }

}
