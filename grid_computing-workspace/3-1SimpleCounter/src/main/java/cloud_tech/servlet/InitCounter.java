package cloud_tech.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/InitCounter")
public class InitCounter extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public InitCounter() {
        super();
    }

    int count;

    public void init() throws ServletException {
      String initial = getInitParameter("initial");
      try {
        count = Integer.parseInt(initial);
      }
      catch (NumberFormatException e) {
        count = 0;
      }
    }

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		 res.setContentType("text/plain");
		    PrintWriter out = res.getWriter();
		    count++;
		    out.println("Since loading (and with a possible initialization");
		    out.println("parameter figured in), this servlet has been accessed");
		    out.println(count + " times.");
	}

}
