package cloud_tech.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SImpleCounter
 */
@WebServlet("/SImpleCounter")
public class SImpleCounter extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SImpleCounter() {
        super();
        // TODO Auto-generated constructor stub
    }

    int count = 0;

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		 res.setContentType("text/plain");
		    PrintWriter out = res.getWriter();
		    count++;
		    out.println("Since loading, this servlet has been accessed " +
		                count + " times.");
	}


}
